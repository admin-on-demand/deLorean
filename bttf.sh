#!/bin/bash

# https://github.com/m10k/toolbox

version() {
  echo "3.0"
}

options() {
  return 0
}

main() {
  log_info "adding options"
  opt_add_arg "D" "days"    "" "1" "How many days to keep. Default: 1" ""
  opt_add_arg "H" "hours"   "" "0" "How many hours to keep. Default: 0" ""
  opt_add_arg "M" "months"  "" "0" "How many months to keep. Default: 0" ""
  opt_add_arg "V" "version" "" "0" "Output version number" "" version
  opt_add_arg "Y" "years"   "" "0" "How many years to keep. Default: 0" ""
  opt_add_arg "s" "source"  "v" "/etc"  "Path to backup from" ""
  opt_add_arg "t" "target"  "" ""  "The name for the backup. Default is source, / replaced with _" ""
  opt_add_arg "u" "user"    "" ""  "username for SSH" ""
  opt_add_arg "n" "hostname" "" "" "hostname for SSH" ""
  opt_add_arg "p" "port"    "" ""  "port for SSH" ""
  opt_parse "$@"
  echo gestartet
  mutex_lock delorean_two
  echo sleeping 5
  sleep 1
  echo sleeping 4
  sleep 1
  echo sleeping 3
  sleep 1
  echo sleeping 2
  sleep 1
  echo sleeping 1
  sleep 1
  echo waking up
  # log_debug "Diese Debugmeldung wird nicht ausgegeben"
  # log_info  "Diese Info wird auch nicht ausgegeben"
  # log_warn  "Diese Warnung wird ausgegeben"
  # log_error "Fehler werden immer ausgegeben"
  # log_set_verbosity 10
  # log_debug "Diese Debugmeldung wird ausgegeben"
  # log_info  "Diese Info wird ausgegeben"
  # seq 1 5 | log_error
  # log_set_verbosity -1
  # log_error "Fehler werden immer ausgegeben"
  mutex_unlock delorean_two
  return 0
}

{
  . toolbox.sh
  # include "log"
  include "opt"
  include "mutex"
  main "$@"
  exit "$?"
}
