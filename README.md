# deLorean
Backup shell script, mimics the behaviour of a time machine by another company. It aims to preserve as much diskspace on the backup media

Usage: 
`deLorean <sourcedir> <backup_rootdir> <target> <method> <years> [months[days[hours[minutes[seconds]]]]]`

`sourcedir` is either a local path like `/home` or a complete ssh connect string like `user@server:/path` depending on method.

`backup_rootdir` is the path above `target`. This allows you to backup to more than one target directories/disks.

`target` is the name for your backup directory. If you create backups from `/` you can call it `slash`.

`method` is one of
- local is for any local path on the machine creating the backups
- ssh:port 
- rsyncssh Depends on properly configured `~/.ssh/config` locally
- rsyncsshsudo This is the most commonly used option. Depends on properly configured `~/.ssh/config` locally and passwordless sudo on the target machine.

`years` `months` `days` `hours` `minutes` `seconds` is the timespan you want to keep the snapshot. After a backup job finishes sucessfully it deletes expired snapshots.

## Example usage

Create local backup of `/home` directory, keep for a week  
`/path/to/deLorean /home/ /path/to/backup home local 0 0 7 0 0 0`

Create backup of remote machine:/etc directory, using rsync and sudo  
`/path/to/deLorean deLorean@some.remote.machine:/etc/ /path/to/backup etc rsyncsshsudo 0 0 7 0 0 0`
